﻿using System;
using System.Windows.Forms;

namespace NoteTreeStorage
{
    public partial class FormDima : Form
    {
        public FormDima()
        {
            InitializeComponent();
            textBox1.Text = Form1.settings.NotesPath;
            checkBox1.Checked = Form1.settings.Backup;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1.settings.NotesPath = textBox1.Text;
            Form1.settings.Backup = checkBox1.Checked;
            Form1.settings.Save();
            Close();
        }
    }
}
