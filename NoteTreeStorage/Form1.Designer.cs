﻿namespace NoteTreeStorage
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.toolFileSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolNodeSave = new System.Windows.Forms.ToolStripButton();
            this.toolNodeRemove = new System.Windows.Forms.ToolStripButton();
            this.toolNodeAddChild = new System.Windows.Forms.ToolStripButton();
            this.toolNodeAddAfter = new System.Windows.Forms.ToolStripButton();
            this.toolNodeClone = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolNodeUp = new System.Windows.Forms.ToolStripButton();
            this.toolNodeDown = new System.Windows.Forms.ToolStripButton();
            this.toolNodeLeft = new System.Windows.Forms.ToolStripButton();
            this.toolNodeRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolTabAdd = new System.Windows.Forms.ToolStripButton();
            this.toolTabRemove = new System.Windows.Forms.ToolStripButton();
            this.toolTabProps = new System.Windows.Forms.ToolStripButton();
            this.toolTabLeft = new System.Windows.Forms.ToolStripButton();
            this.toolTabRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditCut = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditDate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditFind = new System.Windows.Forms.ToolStripMenuItem();
            this.menuView = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNode = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNodeAddAfter = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNodeAddChild = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNodeSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNodeRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuTabAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTabRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTabProps = new System.Windows.Forms.ToolStripMenuItem();
            this.menuService = new System.Windows.Forms.ToolStripMenuItem();
            this.menuServiceSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutAboutAboutUs = new System.Windows.Forms.ToolStripMenuItem();
            this.statusMain = new System.Windows.Forms.StatusStrip();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.toolStripMain.SuspendLayout();
            this.menuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMain
            // 
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolFileSave,
            this.toolStripSeparator1,
            this.toolNodeSave,
            this.toolNodeRemove,
            this.toolNodeAddChild,
            this.toolNodeAddAfter,
            this.toolNodeClone,
            this.toolStripSeparator8,
            this.toolNodeUp,
            this.toolNodeDown,
            this.toolNodeLeft,
            this.toolNodeRight,
            this.toolStripSeparator3,
            this.toolTabAdd,
            this.toolTabRemove,
            this.toolTabProps,
            this.toolTabLeft,
            this.toolTabRight,
            this.toolStripSeparator4});
            this.toolStripMain.Location = new System.Drawing.Point(0, 24);
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.Size = new System.Drawing.Size(933, 25);
            this.toolStripMain.TabIndex = 0;
            this.toolStripMain.Text = "toolStrip1";
            // 
            // toolFileSave
            // 
            this.toolFileSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolFileSave.Image = global::NoteTreeStorage.Properties.Resources.saveFile;
            this.toolFileSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFileSave.Name = "toolFileSave";
            this.toolFileSave.Size = new System.Drawing.Size(23, 22);
            this.toolFileSave.Text = "Save";
            this.toolFileSave.Click += new System.EventHandler(this.eSaveItem);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolNodeSave
            // 
            this.toolNodeSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeSave.Image = global::NoteTreeStorage.Properties.Resources.saveNode;
            this.toolNodeSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeSave.Name = "toolNodeSave";
            this.toolNodeSave.Size = new System.Drawing.Size(23, 22);
            this.toolNodeSave.Text = "SaveItem";
            this.toolNodeSave.Click += new System.EventHandler(this.eSaveItem);
            // 
            // toolNodeRemove
            // 
            this.toolNodeRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeRemove.Image = global::NoteTreeStorage.Properties.Resources.del;
            this.toolNodeRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeRemove.Name = "toolNodeRemove";
            this.toolNodeRemove.Size = new System.Drawing.Size(23, 22);
            this.toolNodeRemove.Text = "Remove selected";
            this.toolNodeRemove.Click += new System.EventHandler(this.eRemoveNode);
            // 
            // toolNodeAddChild
            // 
            this.toolNodeAddChild.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeAddChild.Image = global::NoteTreeStorage.Properties.Resources.addChild;
            this.toolNodeAddChild.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeAddChild.Name = "toolNodeAddChild";
            this.toolNodeAddChild.Size = new System.Drawing.Size(23, 22);
            this.toolNodeAddChild.Text = "Add child";
            this.toolNodeAddChild.Click += new System.EventHandler(this.eAddChildNode);
            // 
            // toolNodeAddAfter
            // 
            this.toolNodeAddAfter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeAddAfter.Image = global::NoteTreeStorage.Properties.Resources.add;
            this.toolNodeAddAfter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeAddAfter.Name = "toolNodeAddAfter";
            this.toolNodeAddAfter.Size = new System.Drawing.Size(23, 22);
            this.toolNodeAddAfter.Text = "Add after";
            this.toolNodeAddAfter.Click += new System.EventHandler(this.eAddAfterNode);
            // 
            // toolNodeClone
            // 
            this.toolNodeClone.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeClone.Image = global::NoteTreeStorage.Properties.Resources.clone;
            this.toolNodeClone.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeClone.Name = "toolNodeClone";
            this.toolNodeClone.Size = new System.Drawing.Size(23, 22);
            this.toolNodeClone.Text = "toolStripButton1";
            this.toolNodeClone.Click += new System.EventHandler(this.eNodeClone);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // toolNodeUp
            // 
            this.toolNodeUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeUp.Image = global::NoteTreeStorage.Properties.Resources.arrow_up;
            this.toolNodeUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeUp.Name = "toolNodeUp";
            this.toolNodeUp.Size = new System.Drawing.Size(23, 22);
            this.toolNodeUp.Text = "toolStripButton1";
            this.toolNodeUp.Click += new System.EventHandler(this.eNodeUp);
            // 
            // toolNodeDown
            // 
            this.toolNodeDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeDown.Image = global::NoteTreeStorage.Properties.Resources.arrow_down;
            this.toolNodeDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeDown.Name = "toolNodeDown";
            this.toolNodeDown.Size = new System.Drawing.Size(23, 22);
            this.toolNodeDown.Text = "toolStripButton2";
            this.toolNodeDown.Click += new System.EventHandler(this.eNodeDown);
            // 
            // toolNodeLeft
            // 
            this.toolNodeLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeLeft.Image = global::NoteTreeStorage.Properties.Resources.arrow_left;
            this.toolNodeLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeLeft.Name = "toolNodeLeft";
            this.toolNodeLeft.Size = new System.Drawing.Size(23, 22);
            this.toolNodeLeft.Text = "toolStripButton3";
            this.toolNodeLeft.Click += new System.EventHandler(this.toolNodeLeft_Click);
            // 
            // toolNodeRight
            // 
            this.toolNodeRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNodeRight.Image = global::NoteTreeStorage.Properties.Resources.arrow_right;
            this.toolNodeRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNodeRight.Name = "toolNodeRight";
            this.toolNodeRight.Size = new System.Drawing.Size(23, 22);
            this.toolNodeRight.Text = "toolStripButton4";
            this.toolNodeRight.Click += new System.EventHandler(this.eNodeRight);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolTabAdd
            // 
            this.toolTabAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolTabAdd.Image = global::NoteTreeStorage.Properties.Resources.addTab;
            this.toolTabAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolTabAdd.Name = "toolTabAdd";
            this.toolTabAdd.Size = new System.Drawing.Size(23, 22);
            this.toolTabAdd.Text = "toolStripButton1";
            this.toolTabAdd.Click += new System.EventHandler(this.eAddTab);
            // 
            // toolTabRemove
            // 
            this.toolTabRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolTabRemove.Image = global::NoteTreeStorage.Properties.Resources.removeTab;
            this.toolTabRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolTabRemove.Name = "toolTabRemove";
            this.toolTabRemove.Size = new System.Drawing.Size(23, 22);
            this.toolTabRemove.Text = "toolStripButton2";
            this.toolTabRemove.Click += new System.EventHandler(this.eRemoveTab);
            // 
            // toolTabProps
            // 
            this.toolTabProps.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolTabProps.Image = global::NoteTreeStorage.Properties.Resources.tab;
            this.toolTabProps.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolTabProps.Name = "toolTabProps";
            this.toolTabProps.Size = new System.Drawing.Size(23, 22);
            this.toolTabProps.Text = "toolStripButton3";
            this.toolTabProps.Click += new System.EventHandler(this.eOpenTabProps);
            // 
            // toolTabLeft
            // 
            this.toolTabLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolTabLeft.Image = global::NoteTreeStorage.Properties.Resources.arrow_left;
            this.toolTabLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolTabLeft.Name = "toolTabLeft";
            this.toolTabLeft.Size = new System.Drawing.Size(23, 22);
            this.toolTabLeft.Text = "toolStripButton2";
            this.toolTabLeft.Click += new System.EventHandler(this.toolTabLeft_Click);
            // 
            // toolTabRight
            // 
            this.toolTabRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolTabRight.Image = global::NoteTreeStorage.Properties.Resources.arrow_right;
            this.toolTabRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolTabRight.Name = "toolTabRight";
            this.toolTabRight.Size = new System.Drawing.Size(23, 22);
            this.toolTabRight.Text = "toolStripButton3";
            this.toolTabRight.Click += new System.EventHandler(this.toolTabRight_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuEdit,
            this.menuView,
            this.menuNode,
            this.menuService,
            this.menuAbout});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(933, 24);
            this.menuMain.TabIndex = 1;
            this.menuMain.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileSave});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(37, 20);
            this.menuFile.Text = "File";
            // 
            // menuFileSave
            // 
            this.menuFileSave.Image = global::NoteTreeStorage.Properties.Resources.saveFile;
            this.menuFileSave.Name = "menuFileSave";
            this.menuFileSave.Size = new System.Drawing.Size(98, 22);
            this.menuFileSave.Text = "Save";
            this.menuFileSave.Click += new System.EventHandler(this.menuFileSave_Click);
            // 
            // menuEdit
            // 
            this.menuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEditUndo,
            this.menuEditRedo,
            this.toolStripSeparator5,
            this.menuEditCut,
            this.menuEditCopy,
            this.menuEditPaste,
            this.toolStripSeparator6,
            this.menuEditDate,
            this.toolStripSeparator7,
            this.menuEditFind});
            this.menuEdit.Name = "menuEdit";
            this.menuEdit.Size = new System.Drawing.Size(39, 20);
            this.menuEdit.Text = "Edit";
            // 
            // menuEditUndo
            // 
            this.menuEditUndo.Name = "menuEditUndo";
            this.menuEditUndo.Size = new System.Drawing.Size(171, 22);
            this.menuEditUndo.Text = "Undo";
            // 
            // menuEditRedo
            // 
            this.menuEditRedo.Name = "menuEditRedo";
            this.menuEditRedo.Size = new System.Drawing.Size(171, 22);
            this.menuEditRedo.Text = "Redo";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(168, 6);
            // 
            // menuEditCut
            // 
            this.menuEditCut.Name = "menuEditCut";
            this.menuEditCut.Size = new System.Drawing.Size(171, 22);
            this.menuEditCut.Text = "Cut";
            // 
            // menuEditCopy
            // 
            this.menuEditCopy.Name = "menuEditCopy";
            this.menuEditCopy.Size = new System.Drawing.Size(171, 22);
            this.menuEditCopy.Text = "Copy";
            // 
            // menuEditPaste
            // 
            this.menuEditPaste.Name = "menuEditPaste";
            this.menuEditPaste.Size = new System.Drawing.Size(171, 22);
            this.menuEditPaste.Text = "Paste";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(168, 6);
            // 
            // menuEditDate
            // 
            this.menuEditDate.Name = "menuEditDate";
            this.menuEditDate.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.menuEditDate.Size = new System.Drawing.Size(171, 22);
            this.menuEditDate.Text = "Insert date";
            this.menuEditDate.Click += new System.EventHandler(this.menuEditDate_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(168, 6);
            // 
            // menuEditFind
            // 
            this.menuEditFind.Name = "menuEditFind";
            this.menuEditFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.menuEditFind.Size = new System.Drawing.Size(171, 22);
            this.menuEditFind.Text = "Find";
            this.menuEditFind.Click += new System.EventHandler(this.menuEditFind_Click);
            // 
            // menuView
            // 
            this.menuView.Name = "menuView";
            this.menuView.Size = new System.Drawing.Size(44, 20);
            this.menuView.Text = "View";
            // 
            // menuNode
            // 
            this.menuNode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNodeAddAfter,
            this.menuNodeAddChild,
            this.menuNodeSave,
            this.menuNodeRemove,
            this.toolStripSeparator2,
            this.menuTabAdd,
            this.menuTabRemove,
            this.menuTabProps});
            this.menuNode.Name = "menuNode";
            this.menuNode.Size = new System.Drawing.Size(43, 20);
            this.menuNode.Text = "Item";
            // 
            // menuNodeAddAfter
            // 
            this.menuNodeAddAfter.Image = global::NoteTreeStorage.Properties.Resources.add;
            this.menuNodeAddAfter.Name = "menuNodeAddAfter";
            this.menuNodeAddAfter.Size = new System.Drawing.Size(190, 22);
            this.menuNodeAddAfter.Text = "Add item";
            this.menuNodeAddAfter.Click += new System.EventHandler(this.eAddAfterNode);
            // 
            // menuNodeAddChild
            // 
            this.menuNodeAddChild.Image = global::NoteTreeStorage.Properties.Resources.addChild;
            this.menuNodeAddChild.Name = "menuNodeAddChild";
            this.menuNodeAddChild.Size = new System.Drawing.Size(190, 22);
            this.menuNodeAddChild.Text = "Add child item";
            this.menuNodeAddChild.Click += new System.EventHandler(this.eAddChildNode);
            // 
            // menuNodeSave
            // 
            this.menuNodeSave.Image = global::NoteTreeStorage.Properties.Resources.saveNode;
            this.menuNodeSave.Name = "menuNodeSave";
            this.menuNodeSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuNodeSave.Size = new System.Drawing.Size(190, 22);
            this.menuNodeSave.Text = "Save this item";
            this.menuNodeSave.Click += new System.EventHandler(this.eSaveItem);
            // 
            // menuNodeRemove
            // 
            this.menuNodeRemove.Image = global::NoteTreeStorage.Properties.Resources.del;
            this.menuNodeRemove.Name = "menuNodeRemove";
            this.menuNodeRemove.Size = new System.Drawing.Size(190, 22);
            this.menuNodeRemove.Text = "Remove selected item";
            this.menuNodeRemove.Click += new System.EventHandler(this.eRemoveNode);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(187, 6);
            // 
            // menuTabAdd
            // 
            this.menuTabAdd.Image = global::NoteTreeStorage.Properties.Resources.addTab;
            this.menuTabAdd.Name = "menuTabAdd";
            this.menuTabAdd.Size = new System.Drawing.Size(190, 22);
            this.menuTabAdd.Text = "Add tab";
            this.menuTabAdd.Click += new System.EventHandler(this.eAddTab);
            // 
            // menuTabRemove
            // 
            this.menuTabRemove.Image = global::NoteTreeStorage.Properties.Resources.removeTab;
            this.menuTabRemove.Name = "menuTabRemove";
            this.menuTabRemove.Size = new System.Drawing.Size(190, 22);
            this.menuTabRemove.Text = "Remove tab";
            this.menuTabRemove.Click += new System.EventHandler(this.eRemoveTab);
            // 
            // menuTabProps
            // 
            this.menuTabProps.Image = global::NoteTreeStorage.Properties.Resources.tab;
            this.menuTabProps.Name = "menuTabProps";
            this.menuTabProps.Size = new System.Drawing.Size(190, 22);
            this.menuTabProps.Text = "Tab properties";
            this.menuTabProps.Click += new System.EventHandler(this.eOpenTabProps);
            // 
            // menuService
            // 
            this.menuService.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuServiceSettings});
            this.menuService.Name = "menuService";
            this.menuService.Size = new System.Drawing.Size(56, 20);
            this.menuService.Text = "Service";
            // 
            // menuServiceSettings
            // 
            this.menuServiceSettings.Image = global::NoteTreeStorage.Properties.Resources.settings;
            this.menuServiceSettings.Name = "menuServiceSettings";
            this.menuServiceSettings.Size = new System.Drawing.Size(152, 22);
            this.menuServiceSettings.Text = "Settings";
            this.menuServiceSettings.Click += new System.EventHandler(this.eOpenSettings);
            // 
            // menuAbout
            // 
            this.menuAbout.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutAboutAboutUs});
            this.menuAbout.Name = "menuAbout";
            this.menuAbout.Size = new System.Drawing.Size(52, 20);
            this.menuAbout.Text = "About";
            // 
            // aboutAboutAboutUs
            // 
            this.aboutAboutAboutUs.Name = "aboutAboutAboutUs";
            this.aboutAboutAboutUs.Size = new System.Drawing.Size(122, 22);
            this.aboutAboutAboutUs.Text = "About us";
            this.aboutAboutAboutUs.Click += new System.EventHandler(this.eOpenAboutForm);
            // 
            // statusMain
            // 
            this.statusMain.Location = new System.Drawing.Point(0, 344);
            this.statusMain.Name = "statusMain";
            this.statusMain.Size = new System.Drawing.Size(933, 22);
            this.statusMain.TabIndex = 2;
            this.statusMain.Text = "statusStrip1";
            // 
            // tabControlMain
            // 
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 49);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(933, 295);
            this.tabControlMain.TabIndex = 3;
            this.tabControlMain.SelectedIndexChanged += new System.EventHandler(this.eOnTabChange);
            this.tabControlMain.DoubleClick += new System.EventHandler(this.eOpenTabProps);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 366);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.statusMain);
            this.Controls.Add(this.toolStripMain);
            this.Controls.Add(this.menuMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "NoteTreeStorage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.eSaveItem);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMain;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.StatusStrip statusMain;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuAbout;
        private System.Windows.Forms.ToolStripMenuItem menuFileSave;
        private System.Windows.Forms.ToolStripButton toolNodeSave;
        private System.Windows.Forms.ToolStripButton toolFileSave;
        private System.Windows.Forms.ToolStripMenuItem menuNode;
        private System.Windows.Forms.ToolStripMenuItem menuNodeSave;
        private System.Windows.Forms.ToolStripMenuItem menuNodeAddAfter;
        private System.Windows.Forms.ToolStripMenuItem menuNodeRemove;
        private System.Windows.Forms.ToolStripMenuItem menuNodeAddChild;
        private System.Windows.Forms.ToolStripMenuItem aboutAboutAboutUs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolNodeAddChild;
        private System.Windows.Forms.ToolStripButton toolNodeAddAfter;
        private System.Windows.Forms.ToolStripButton toolNodeRemove;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuTabAdd;
        private System.Windows.Forms.ToolStripMenuItem menuTabRemove;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolTabAdd;
        private System.Windows.Forms.ToolStripButton toolTabRemove;
        private System.Windows.Forms.ToolStripButton toolTabProps;
        private System.Windows.Forms.ToolStripMenuItem menuTabProps;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.ToolStripMenuItem menuEdit;
        private System.Windows.Forms.ToolStripMenuItem menuView;
        private System.Windows.Forms.ToolStripMenuItem menuService;
        private System.Windows.Forms.ToolStripMenuItem menuServiceSettings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolNodeUp;
        private System.Windows.Forms.ToolStripButton toolNodeDown;
        private System.Windows.Forms.ToolStripButton toolNodeLeft;
        private System.Windows.Forms.ToolStripButton toolNodeRight;
        private System.Windows.Forms.ToolStripMenuItem menuEditUndo;
        private System.Windows.Forms.ToolStripMenuItem menuEditRedo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem menuEditCut;
        private System.Windows.Forms.ToolStripMenuItem menuEditCopy;
        private System.Windows.Forms.ToolStripMenuItem menuEditPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem menuEditDate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem menuEditFind;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolStripButton toolNodeClone;
        private System.Windows.Forms.ToolStripButton toolTabLeft;
        private System.Windows.Forms.ToolStripButton toolTabRight;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;

    }
}

