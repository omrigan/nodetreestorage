﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using NoteTreeStorage.Properties;

namespace NoteTreeStorage
{
    public partial class Form1 : Form
    {
       

        public static MyTab selectedTab = new MyTab();
        FormFind fF = new FormFind();
        public static MyFile file;
        public static MySettings settings;
        public static int currentXmlVersion = 1;
        public static Form1 me;
        public static TabControl tabControlMainPub;
        public static ContextMenu moveMenu;
        public Form1()
        {   
            
            InitializeComponent();
            menuAbout.Text = Resources.menu_About;
            Closing += eSaveItem;
            me = this;
            settings = new MySettings(Resources.path_settingsFolder + "/settings.ini");
            readFile(settings.NotesPath);
            tabControlMainPub = tabControlMain;
            #region ContextMenu
            this.ContextMenu = new ContextMenu();
            this.ContextMenu.MenuItems.Add(new MenuItem(Resources.menu_TabProps, selectedTab.eTabPageProperties));
            this.ContextMenu.MenuItems.Add(new MenuItem(Resources.menu_TabAdd, eAddTab));
            this.ContextMenu.MenuItems.Add(new MenuItem(Resources.menu_TabRemove, eRemoveTab));

            #endregion
            updateMoveMenu();
            #region NameChanges

            
            #endregion
        }

        public static void updateMoveMenu()
        {
            moveMenu = file.GetMoveMenu();
        }

        public void ShowErrorMessage()
        {
            
        }
        private void readFile(string path)
        {
            
            try
            {
                if (settings.Backup)
                {
                    System.IO.File.Copy(path, path.Substring(0, path.Length - 5).ToString() + "-backup.note",true);
                }
                file = MyIO.readFile(path);
            }
            catch (Exception e)
            {
                if (MessageBox.Show("File " + path + " damaged or not exists. Rewrite? If you want to change file name, go to the files/MySetting.ini. " + e.ToString(), "File exepction", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                   
                    
                    System.IO.Directory.CreateDirectory((new System.IO.FileInfo(path)).DirectoryName);
                    
                    file = new MyFile();
                    file.Add(new MyTab(Resources.name_NewTab));
                    file[0].Children.Add(new MyNode(file[0], Resources.name_NewNode));
                }
                else
                {
                    this.Close();
                    Application.Exit();
                    return;
                }
            }
            foreach (MyTab tab in file)
            {
                tab.init();
                tabControlMain.Controls.Add(tab.TabPage);
            }

            eOnTabChange();
            
        }

       
        private void Form1_Load(object sender, EventArgs e)
        {
         
            

        }
        public static void SaveFile()
        {
            MyIO.writeFile(settings.NotesPath, file);
        }

        public void eSaveItem(object sender, EventArgs e)
        {
            selectedTab.eSaveItem();
        }
        public void eTreeNodeChangeColor(object sender, EventArgs e)
        {
           
               if(colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK){
                   selectedTab.SelectedNode.TreeNode.BackColor = colorDialog1.Color;
                   selectedTab.NameBox.BackColor = colorDialog1.Color;
                   SaveFile();
               }
        }
        
        private void eOnTabChange(object sender=null, EventArgs e=null)
        {
            selectedTab= (MyTab) tabControlMain.SelectedTab.Tag;
           
        }
        public void eAddAfterNode(object senders, EventArgs e)
        {

            selectedTab.SelectedNode.Parent.AddChild(new MyNode(selectedTab.SelectedNode.Parent, Resources.name_NewNode, Resources.name_NewText));
            SaveFile();
            
        }
        public void eAddChildNode(object sender, EventArgs e)
        {
            selectedTab.SelectedNode.AddChild(new MyNode(selectedTab.SelectedNode, Resources.name_NewNode, Resources.name_NewText));
            SaveFile();

        }

        public void eRemoveNode(object sender, EventArgs e)
        { 
            if (selectedTab.SelectedNode.CanBeDeleted())
            {
                if (MessageBox.Show(Resources.mess_NodeDeletingConfirm_1 + selectedTab.SelectedNode.Name + Resources.mess_NodeDeletingConfirm_2, Resources.mess_NodeDelete, MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    selectedTab.SelectedNode.Remove();
                }
            }
            else
            {
                MessageBox.Show(Form1.me, Resources.error_RemoveLastNode, Resources.error_ErrorOccurred);
            }
            SaveFile();
        }
        private void eAddTab(object sender, EventArgs e)
        {
            MyTab newTab = new MyTab(Resources.name_NewTab);
            newTab.Children.Add(new MyNode(newTab, Resources.name_NewNode));
            file.Add(newTab);
            newTab.init();
            tabControlMain.Controls.Add(newTab.TabPage);
            tabControlMain.SelectedTab = newTab.TabPage;
            updateMoveMenu();

        }
        private void eRemoveTab(object sender, EventArgs e)
        {
            
                if (tabControlMain.Controls.Count > 1)
                {
                    if (MessageBox.Show(Resources.mess_NodeDeletingConfirm_1 + selectedTab.Name + Resources.mess_NodeDeletingConfirm_2, Resources.mess_NodeDelete, MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        file.Remove(selectedTab);
                        tabControlMain.Controls.Remove(selectedTab.TabPage);
                        selectedTab.eSaveItem();
                    }
                }
                updateMoveMenu();
            
        }

        

        private void eOpenTabProps(object sender, EventArgs e)
        {
            FormZak fP = new FormZak(selectedTab);
            fP.ShowDialog();
            SaveFile();
            updateMoveMenu();
        }
        private void eOpenAboutForm(object sender, EventArgs e)
        {
            AboutBox1 aB = new AboutBox1();
            aB.ShowDialog();
        }
        private void eOpenSettings(object sender, EventArgs e)
        {
            FormDima fD = new FormDima();
            fD.ShowDialog();
        }
        //FIXME: refactor code below!
        private void eNodeUp(object sender, EventArgs e)
        {
            MyNode sNode = selectedTab.SelectedNode;
            if (selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.IndexOf(sNode.TreeNode) != 0)
            {
                int index = selectedTab.SelectedNode.Index;
                MyNode pNode = selectedTab.SelectedNode.Parent.Children[index - 1];
                selectedTab.SelectedNode.Parent.Children[index - 1] = sNode;
                selectedTab.SelectedNode.Parent.Children[index] = pNode;


                //index = selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.IndexOf(sNode.TreeNode);
                selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.Remove(pNode.TreeNode);
                selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.Insert(index, pNode.TreeNode);
            }
            
            
        }

        private void eNodeDown(object sender, EventArgs e)
        {  
            MyNode sNode = selectedTab.SelectedNode;
            if (selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.IndexOf(sNode.TreeNode) != selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.Count - 1)
            {
                //int index = selectedTab.SelectedNode.Parent.Children.FindIndex(x => x == sNode);
                int index = selectedTab.SelectedNode.Index;
                MyNode pNode = sNode.Parent.Children[index + 1];
                selectedTab.SelectedNode.Parent.Children[index + 1] = sNode;
                selectedTab.SelectedNode.Parent.Children[index] = pNode;


                
                selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.Remove(pNode.TreeNode);
                selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.Insert(index, pNode.TreeNode);
        }
           
            
        }

        private void eNodeRight(object sender, EventArgs e)
        {   MyNode sNode = selectedTab.SelectedNode;
        if (selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.IndexOf(sNode.TreeNode) != 0)
            {

                int index = selectedTab.SelectedNode.Index;;
                MyNode pNode = sNode.Parent.Children[index - 1];
                sNode.Parent.Children.Remove(sNode);
                pNode.Children.Add(sNode);
                sNode.Parent = pNode;

                sNode.Parent.ChildTreeNodesCollection.Remove(sNode.TreeNode);
                pNode.TreeNode.Nodes.Add(sNode.TreeNode);
                pNode.TreeNode.Expand();
                selectedTab.TreeView.SelectedNode = sNode.TreeNode;
            }

        }

        private void toolNodeLeft_Click(object sender, EventArgs e)
        {
            MyNode sNode = selectedTab.SelectedNode;
            if (selectedTab.SelectedNode.Parent.GetType() == selectedTab.SelectedNode.GetType())
            {   
                MyNode pNode = (MyNode)sNode.Parent;
                
                MyBaseNode ppNode = pNode.Parent;
                sNode.Parent.Children.Remove(sNode);
                ppNode.Children.Insert(pNode.Index + 1, sNode);
                sNode.Parent = ppNode;

                sNode.Parent.ChildTreeNodesCollection.Remove(sNode.TreeNode);
                ppNode.ChildTreeNodesCollection.Insert(pNode.Index  + 1, sNode.TreeNode);
                
                selectedTab.TreeView.SelectedNode = sNode.TreeNode;
            }
        }

        private void menuFileSave_Click(object sender, EventArgs e)
        {

        }
        //FIXME: allow ctrl+z in NameBox
        private void menuEditDate_Click(object sender, EventArgs e)
        {
            if(selectedTab.HtmlEditor.Focused)
            {
                 selectedTab.HtmlEditor.SelectedText = DateTime.Now.ToLongDateString();
            }
            if(selectedTab.NameBox.Focused)
            {
                selectedTab.NameBox.Text = "";
                selectedTab.NameBox.SelectedText = DateTime.Now.ToLongDateString();
            }  
            
        }

        private void menuEditFind_Click(object sender, EventArgs e)
        {
            if (fF.Visible)
            {
                fF.Activate();
            }
            else
            {   fF=new FormFind();
                fF.Show();
            }
            
            
        }

        private void eNodeClone(object sender, EventArgs e)
        {
            selectedTab.SelectedNode.Parent.AddChild(selectedTab.SelectedNode.Clone());
        }

        private void toolTabLeft_Click(object sender, EventArgs e)
        {
            MyTab sTab = selectedTab;
            if (tabControlMain.TabPages.IndexOf(sTab.TabPage) != 0)
            {
                int index = tabControlMain.TabPages.IndexOf(sTab.TabPage); ;
                MyTab pTab = file[index - 1];
                file[index - 1] = sTab;
                file[index] = pTab;


                //index = selectedTab.SelectedNode.Parent.ChildTreeNodesCollection.IndexOf(sNode.TreeNode);
                tabControlMain.TabPages.Remove(pTab.TabPage);
                tabControlMain.TabPages.Insert(index, pTab.TabPage);
                SaveFile();
                updateMoveMenu();
            }
        }

        private void toolTabRight_Click(object sender, EventArgs e)
        {
            MyTab sTab = selectedTab;
            if (tabControlMain.TabPages.IndexOf(sTab.TabPage) != tabControlMain.TabPages.Count-1)
            {
                int index = tabControlMain.TabPages.IndexOf(sTab.TabPage);
                MyTab nTab = file[index + 1];
                file[index + 1] = sTab;
                file[index] = nTab;


                
                {
                    tabControlMain.TabPages.Remove(nTab.TabPage);
                    tabControlMain.TabPages.Insert(index, nTab.TabPage);
                }
                SaveFile();
                updateMoveMenu();
            }
        }
    }
}
