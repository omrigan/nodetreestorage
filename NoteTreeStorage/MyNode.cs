﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace NoteTreeStorage
{
    public class MyNode :MyBaseNode
    {
        private TreeNode _treeNode;
        private string _text;
        private readonly bool _isExpanded;
        private readonly Color _color;
        private MyBaseNode _parent;
        

        public MyNode(MyBaseNode parent, string name = "", string text = "", bool expanded=false, Color color = new Color(), List<MyNode> children = null)
            : base(name, children)
        {
            _color = color;
            _text = text;
            _parent = parent;
            _isExpanded = expanded;

        }
        public TreeNode TreeNode {
            get { return _treeNode; }
            set { _treeNode = value; }
        }
        public bool Expanded
        {
            get { return _treeNode.IsExpanded; }
          //  set { _expanded = value; }
        }
        public Color Color
        {
            get { return _treeNode.BackColor; }
            //  set { _expanded = value; }
        }
        public MyBaseNode Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }
        public int Index
        {
            get { return _parent.ChildTreeNodesCollection.IndexOf(TreeNode); }
            
        }
       
        public override TreeNodeCollection ChildTreeNodesCollection
        {
            get{
                return _treeNode.Nodes;
            }
        }
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
        
        public override void init()
        {   _treeNode = new TreeNode {Text = Name};

            _treeNode.ContextMenu = Form1.moveMenu;
            foreach (MyNode n in Children)
            {
                n.init();
                TreeNode.Nodes.Add(n.TreeNode);

            }
            if (_isExpanded) _treeNode.Expand();
            _treeNode.BackColor = _color;
            _treeNode.Tag = this;
            
            
        }

        public bool CanBeDeleted()
        {
            return _parent.GetType() == GetType() ||_parent.Children.Count > 1;
        }
        public void Remove()
        {
              _parent.RemoveChild(this);       
        }
        public MyNode Clone()
        {   
           return new MyNode(_parent, Name, _text, Expanded, _color, Children);
            
            
        }
        public MyNode FindNode(string str, bool searchInNames, bool searchInText)
        {
            if (MyStatic.alreadyReachedSelected)
            {
                if (searchInNames)
                {
                    if (Name.ToLower().Contains(str.ToLower()))
                    {
                        return this;
                    }
                }
                if (searchInText)
                {
                    if (_text.ToLower().Contains(str.ToLower()))
                    {
                        return this;
                    }
                }
            }
            else
            {
                if (Form1.selectedTab.SelectedNode == this)
                {
                    MyStatic.alreadyReachedSelected = true;
                }
            }
            
            return FindBaseNode(str, searchInNames, searchInText);
            
         }
    }
}
