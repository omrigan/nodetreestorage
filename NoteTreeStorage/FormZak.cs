﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteTreeStorage
{
    public partial class FormZak : Form
    {
        MyTab _tab;
        public FormZak(MyTab tab)
        {   InitializeComponent();
            _tab = tab;
            textBox1.Text = _tab.TabPage.Text;
            
        }

        private void FormZak_Load(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {


            save();
        }
        private void save()
        {
            _tab.Name = textBox1.Text;//FIXME can be moved into prop change
            _tab.TabPage.Text = textBox1.Text;
            this.Close();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                save();
            }
        }

       
    }
}
