﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NoteTreeStorage.Properties;

namespace NoteTreeStorage
{
    public class MyFile : List<MyTab>
    {
        public ContextMenu GetMoveMenu()
        {
            ContextMenu menu = new ContextMenu();
            menu.MenuItems.Add(new MenuItem(Resources.menu_NodeAddAfter, Form1.me.eAddAfterNode));
            menu.MenuItems.Add(new MenuItem(Resources.menu_NodeAddChild, Form1.me.eAddChildNode));
            menu.MenuItems.Add(new MenuItem(Resources.menu_NodeRemove, Form1.me.eRemoveNode));
            MenuItem item = new MenuItem(Resources.menu_NodeMove);
            foreach (var t in this)
            {
                MenuItem it = new MenuItem(t.Name,t.eAttractSelectedNode);
                item.MenuItems.Add(it);
            }
            menu.MenuItems.Add(item);
            Form1.me.ContextMenu = menu;
            return menu;
        }
    }
}
