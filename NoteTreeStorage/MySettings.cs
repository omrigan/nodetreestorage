﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nini.Config;
using NoteTreeStorage.Properties;

namespace NoteTreeStorage
{
    public class MySettings
    {
        private IConfigSource _source;
        public MySettings(string path)
        {
            try
            {
                _source = new IniConfigSource(path);
            }catch(Exception)
            {
                System.IO.Directory.CreateDirectory((new System.IO.FileInfo(path)).DirectoryName);
                System.IO.File.Create(path).Close();
                _source = new IniConfigSource(path);
                _source.Configs.Add("Notes");
                _source.Configs.Add("Other");
                _source.Save();
            }
            
        }

	    public string NotesPath
	    {
            get { return _source.Configs["Notes"].GetString("NotePath", Resources.path_dataFolder +  @"\my.note");  }
            set { _source.Configs["Notes"].Set("NotePath", value);}
	    }
        public bool Backup
        {
            get { return _source.Configs["Other"].GetBoolean("Backup", true); }
            set { _source.Configs["Other"].Set("Backup", value); }
        }
        public void Save(){
            _source.Save();
        }
    }
	
}
