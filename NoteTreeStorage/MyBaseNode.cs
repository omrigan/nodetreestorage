﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using MSDN.Html.Editor;

namespace NoteTreeStorage
{
    public abstract class MyBaseNode
    {
        protected MyBaseNode(string name = "", List<MyNode> children=null)
        {
            Name = name;
            Children = children ?? new List<MyNode>();
        }

        public List<MyNode> Children { get; set; }

        public string Name { get; set; }

        public abstract TreeNodeCollection ChildTreeNodesCollection
         {
             get;
         }
         public abstract void init();
         public void AddChild(MyNode newNode)
         {
             if (Children.Count > 0)
             {
                 Children.Insert(Form1.selectedTab.SelectedNode.Index + 1, newNode);

             }
             else
             {
                 Children.Add(newNode);
             }
             newNode.init();
             ChildTreeNodesCollection.Insert(Form1.selectedTab.SelectedNode.Index+1,newNode.TreeNode);
             Form1.selectedTab.TreeView.SelectedNode = newNode.TreeNode;
             Form1.selectedTab.NameBox.Focus();
             

         }
         
         //public abstract void remove();
         public void RemoveChild(MyNode node)
         {
             Children.Remove(node);
             ChildTreeNodesCollection.Remove(node.TreeNode);
         }
        
         

         public MyNode FindBaseNode(string str, bool searchInNames, bool searchInText)
         {
             return Children.Select(mN => mN.FindNode(str, searchInNames, searchInText)).FirstOrDefault(nMc => nMc != null);
         }
    }
}
