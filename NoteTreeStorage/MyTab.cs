﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MSDN.Html.Editor;
using NoteTreeStorage.Properties;

namespace NoteTreeStorage
{
    public class MyTab : MyBaseNode
    {   
        private TabPage _tabPage;
        private TreeView _treeView;
        //private RichTextBox _rTextBox;
        private TextBox _nameBox;
        private MyNode _selectedNode;
        private HtmlEditorControl _htmlEditor;
        
        
        public MyTab(string name = "", List<MyNode> children = null)
            : base(name, children)
        { }
        public TabPage TabPage
        {
            get { return _tabPage; }
        }
        public TreeView TreeView
        {
            get { return _treeView; }
        }
        public HtmlEditorControl HtmlEditor
        {
            get { return _htmlEditor; }
        }
        
        public TextBox NameBox
        {
            get { return _nameBox; }
        }
        public MyNode SelectedNode
        {
            get { return _selectedNode; }
        }
        public override void init()
        {
            _tabPage = new TabPage {Text = Name, Tag = this};
            _tabPage.Click += eTabPageProperties;
            
            _treeView = new TreeView {Dock = DockStyle.Fill, Name = "TreeView"};
            _treeView.AfterSelect += eTreeViewOnSelect;
            _treeView.DoubleClick += Form1.me.eTreeNodeChangeColor;
            foreach (MyNode n in Children)
            {
                n.init();
                _treeView.Nodes.Add(n.TreeNode);

            }

            
            _nameBox = new TextBox {Dock = DockStyle.Top};
            _nameBox.KeyUp += eSaveItem;

            _htmlEditor = new HtmlEditorControl { Dock = DockStyle.Fill};
            _htmlEditor.StylesheetUrl =
            "file:///" + Application.StartupPath + Resources.path_settingsFolder + "/main.css";
           
           
            if (_treeView.Nodes.Count > 0)
            {
                
                _selectedNode = Children[0];
                _htmlEditor.InnerHtml = _selectedNode.Text;
                _nameBox.Text = _selectedNode.Name;
                _treeView.SelectedNode = _treeView.Nodes[0];
                
            }

            #region ContextMenu

            _treeView.ContextMenu = Form1.moveMenu;
            
            
            #endregion
            var splitContainer = new SplitContainer {Dock = DockStyle.Fill};
            splitContainer.Panel1.Controls.Add(_treeView);
            //splitContainer.Panel2.Controls.Add(_rTextBox);
            splitContainer.Panel2.Controls.Add(_htmlEditor);
            splitContainer.Panel2.Controls.Add(_nameBox);
            _tabPage.Controls.Add(splitContainer);            
        }

      
        
        public override TreeNodeCollection ChildTreeNodesCollection
        {
            get
            {
                return _treeView.Nodes;
            }
        }
        private void eTreeViewOnSelect(object sender, TreeViewEventArgs e)
        {   //NOTE: ask someone is it right way?
            eSaveItem();
            _selectedNode  = (MyNode)_treeView.SelectedNode.Tag;
            _htmlEditor.InnerHtml = _selectedNode.Text;
            _nameBox.Text = _selectedNode.Name;
            _nameBox.BackColor = _selectedNode.Color;
        }
        public void eAttractSelectedNode(object sender, EventArgs e)
        {
            MyNode node = Form1.selectedTab.SelectedNode;
            if (node.CanBeDeleted())
            {
               
                node.Remove();
                Children.Add(node);
                _treeView.Nodes.Add(node.TreeNode);

            }
            else
            {
                MessageBox.Show(Form1.me, Resources.error_RemoveLastNode,Resources.error_ErrorOccurred);
            }
            
            
        }
        
        public void eTabPageProperties(object sender = null, EventArgs e = null)
        {
            FormZak fZ = new FormZak(this);
            fZ.ShowDialog();
        }
        public void eSaveItem(object sender=null, KeyEventArgs e=null)
        {
            _selectedNode.Text = _htmlEditor.InnerHtml;
            _selectedNode.Name = _nameBox.Text;
            if (_selectedNode.TreeNode != null)
            {
                _selectedNode.TreeNode.Text = _nameBox.Text;
            }
            Form1.SaveFile();//MARK: eSaveItem file always
        }
        
        
    }
}
