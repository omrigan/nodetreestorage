﻿using System;
using System.Windows.Forms;

namespace NoteTreeStorage
{
    public partial class FormFind : Form
    {
        public FormFind()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
                foreach (MyTab mT in Form1.file)
                { 
                    
                    if (radioButton1.Checked)
                    {
                        
                        MyStatic.alreadyReachedSelected = true;
                        if (checkMyNode(mT.FindBaseNode(textBox1.Text, checkBox1.Checked, checkBox2.Checked), mT)) {
                            return;
                        }
                    }
                    else
                    {//FIXME
                        MyStatic.alreadyReachedSelected = false;
                        if (checkMyNode(mT.FindBaseNode(textBox1.Text, checkBox1.Checked, checkBox2.Checked), mT)) {
                            return;
                        }

                    }
                }

                radioButton1.Checked = true;
            MessageBox.Show("Nothing!");
        }
        private bool checkMyNode(MyNode mN, MyTab mT){
            if (mN != null)
            {
                Form1.tabControlMainPub.SelectedTab = mT.TabPage;
                mT.TreeView.SelectedNode = mN.TreeNode;
                Form1.me.Activate();
                radioButton2.Checked = true;
                return true;
            }
            return false;
        }
    }
}
